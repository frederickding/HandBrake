stages:
  - build
  - build_gui

variables:
  SIGNINSTALLER: "false"
  SIGNTHUMBPRINT: ""
  SIGNTOOLLOCATION: ""
  SIGNTIMESTAMP: ""
  SIGNTIMESTAMPSERVER: ""
  SIGNFLATPAK: "false"
  SIGNPGPKEY: "" # set in secret variables as file; passphrase not supported
  BUILDFDK: "false" # this flag controls whether or not to build with AAC
  MSBUILDPATH: "C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\BuildTools\\MSBuild\\Current\\Bin"

# this is only needed if building for Linux
.fedora_flatpak: &using_fedora_flatpak
  image: registry.fedoraproject.org/fedora
  tags:
    - privileged
  allow_failure: true
  before_script:
    - uname -a && cat /etc/os-release && cat /proc/cpuinfo
    - dnf update -y
    - dnf install -y autoconf automake bzip2 cmake flatpak flatpak-builder fuse gcc-c++ git libtool m4 make meson nasm ninja-build pax perl-Digest-SHA python python3 numactl-devel
    - git config --global --add safe.directory $CI_PROJECT_DIR
    - flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
    - flatpak install -y --noninteractive flathub org.freedesktop.Sdk//23.08
    - flatpak install -y --noninteractive flathub org.freedesktop.Platform//23.08
    - flatpak install -y --noninteractive flathub org.gnome.Sdk//45
    - flatpak install -y --noninteractive flathub org.gnome.Platform//45
    - flatpak install -y --noninteractive flathub org.freedesktop.Sdk.Extension.llvm16//23.08
    - test -f "$SIGNPGPKEY" && (gpg2 --batch --import "$SIGNPGPKEY") && gpg2 --list-secret-keys
    - test $SIGNFLATPAK = "true" && export PGP_ID=$(gpg2 --list-secret-keys --with-colons 2>/dev/null | grep -E '(sec|ssb)[^#]+$' | head -n1 | cut -d ':' -f5) && printf "Do sign flatpak with key ID %s \n" "$PGP_ID"

# "prebuilt" just means that we build the mingw-w64-x86_64 cross compilation toolchain 
# separately and reuse that toolchain, saving time and computation here
# if you are using a fork of this repository, you can save time by changing the line below to
# image: registry.gitlab.com/frederickding/handbrake-builder:master
.fedora_mingw_prebuilt: &using_fedora_mingw_prebuilt
  image: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/handbrake-builder:master
  tags:
    - linux
  before_script:
    - test -d $TOOLCHAIN_PATH && ls $TOOLCHAIN_PATH
    - env | grep VERSION

build_linux:
  <<: *using_fedora_flatpak
  stage: build
  script:
    - scripts/repo-info.sh | tee version.txt
    - test $BUILDFDK = "true" && echo "Building with FDK (license warning)" || echo "Building default configuration"
    - test $BUILDFDK = "true" && ./configure --enable-fdk-aac --flatpak || ./configure --flatpak
    - test $BUILDFDK = "true" && sed -i 's|"--flatpak"|\0, "--enable-fdk-aac"|' pkg/linux/flatpak/*.json
    - cd build
    - make pkg.create.flatpak
  artifacts:
    paths:
      - build/pkg/flatpak/
    when: always
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" || $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - .gitlab-ci.yml
        - pkg/linux/**/*
      allow_failure: true
    - if: $CI_PIPELINE_SOURCE != "push" && $CI_PIPELINE_SOURCE != "merge_request_event"
      when: manual

# a prebuilt Docker image with the cross-compilation toolchain is required
# non-prebuilt toolchain no longer supported by this CI script, due to time/CPU utilization
build_windows:
  <<: *using_fedora_mingw_prebuilt
  stage: build
  script:
    - test $BUILDFDK = "true" && echo "Building with FDK (license warning)" || echo "Building default configuration"
    - test $BUILDFDK = "true" && export FDK="--enable-fdk-aac"
    - ./configure --cross=x86_64-w64-mingw32 --disable-qsv $FDK --launch-jobs=$(nproc) --launch
  dependencies: []
  artifacts:
    paths:
      - build/libhb/
      - build/log/
      - build/project/
      - build/test/
      - build/GNUmakefile
      - build/HandBrakeCLI.exe
      - build/distfile.cfg
    when: always
  cache:
    paths:
      - download/
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" || $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - pkg/linux/**/*
      when: manual
      allow_failure: true
    - if: $CI_PIPELINE_SOURCE == "web" || $CI_PIPELINE_SOURCE == "webide"
      when: manual
    - if: $CI_PIPELINE_SOURCE != "web" && $CI_PIPELINE_SOURCE != "webide"

# this builds the Windows GUI -- requires a properly configured 
# runner on Windows with the Visual Studio 2019 build tools
# comment out this section if you don't have that set up

# for now, PowerShell build requires Pscx
# install with `Install-Module Pscx -Scope AllUsers` on the runner
build_windows_gui:
  stage: build_gui
  needs: [build_windows]
  dependencies:
    - build_windows
  tags:
    - msbuild
    - windows
  before_script:
    - cd win\CS
    - nuget restore
  script:
    - mkdir HandBrakeWPF\bin\publish
    - copy ..\..\build\libhb\hb.dll HandBrakeWPF\bin\publish\hb.dll
    - $env:Path += ";$env:MSBUILDPATH"
    - If($env:SIGNINSTALLER) { msbuild build.xml /t:x64 /p:Profile=Nightly /p:SignToolLocation="$env:SIGNTOOLLOCATION" /p:SignThumbprint="$env:SIGNTHUMBPRINT" /p:SignTimestamp="$env:SIGNTIMESTAMP" /p:SignTimestampServer="$env:SIGNTIMESTAMPSERVER" } else { msbuild build.xml /t:x64 /p:Profile=Nightly }
    - move HandBrakeWPF\bin\publish\HandBrake-*Win_GUI.* ..\..
  artifacts:
    paths:
      - HandBrake-*Win_GUI.*
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" || $CI_PIPELINE_SOURCE == "merge_request_event"
      changes:
        - pkg/linux/**/*
      when: never
      allow_failure: true
    - if: $CI_PIPELINE_SOURCE != "push" && $CI_PIPELINE_SOURCE != "merge_request_event"
      when: manual
    - if: $CI_PIPELINE_SOURCE == "push" || $CI_PIPELINE_SOURCE == "merge_request_event"
